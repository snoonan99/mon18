(ns ^:figwheel-no-load mon18.dev
  (:require
    [mon18.core :as core]
    [devtools.core :as devtools]))


(enable-console-print!)

(devtools/install!)

(core/init!)
