(ns mon18.prod
  (:require
    [mon18.core :as core]))

;;ignore println statements in prod
(set! *print-fn* (fn [& _]))

(core/init!)
