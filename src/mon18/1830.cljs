(ns mon18.gc1830)

(def gamecfg-init
{
    :ver 0
    :player-start-cash [0  0  0  800  600  480  400]
    :bank [0  0  0  12000  12000  12000  12000]
    :paper-limit [0  0  0  20  16  13  11]
    :co-limit 60
    :pool-limit 50
    :cap :full
    :cap-float 60
    :sr-order "sb"
    :trains {
        :limit [4  4  3  2  2  2]
        :stock [
            { :train ["2"]  :price [80]         :count 6 }
            { :train ["3"]  :price [180]        :count 5 }
            { :train ["4"]  :price [300]        :count 4 }
            { :train ["5"]  :price [450]        :count 3 }
            { :train ["6"]  :price [630]        :count 2 }
            { :train ["D"]  :price [[1100 800]] :count 6 }
        ]
    }
    :privates [
        { :id :SV :name "Schuylkill Valley" :price 20   :income 5}
        { :id :C&sL :name "Champlain & St. Lawrence" :price 40 :income 10}
        { :id :D&H :name "Delaware & Hudson" :price 70   :income 15}
        { :id :M&H :name "Mohawk & Hudson"   :price 110  :income 20}
        { :id :C&A :name "Camden & Amboy"    :price 160  :income 25
          :with-share ["PRR" 1]}
        { :id :B&O :name "Baltimore & Ohio"  :price 220  :income 30
          :with-share ["B%O" 0]}
    ]
    :share-cos {
        :PRR  { :name "PRR" :stations 3}
        :NYC  { :name "NYC" :stations 4}
        :CPR  { :name "CPR" :stations 4}
        :B&O  { :name "B&O" :stations 3}
        :C&O  { :name "C&O" :stations 4}
        :Erie { :name "Erie" :stations 3}
        :NYNH { :name "NYNH" :stations 4}
        :B&M  { :name "B&M" :stations 4}
    }
    :shares "pssssssss"
    :share-count [10]
    :market {
        :grid [
           [60 67 71 76 82 90 100 112 126 142 160 180 200 225 250 275 300 325 350]
           [53 60 66 70 76 82  90 100 112 126 142 160 180 200 220 240 260 280 300]
           [46 55 60 65 70 76  82  90 100 111 126 140 155 170 190 210]
           [39 48 54 60 66 71  76  82  90 100 110 126 138]
           [32 41 48 55 62 67  71  76  82  90 100]
           [25 34 42 50 58 65  67  71  75  80]
           [18 27 36 45 54 63  67  69  70]
           [10 20 30 40 50 60  67  68]
           [ 0 10 20 30 40 50  60]
           [ 0  0 10 20 30 40  50]
           [ 0  0  0 10 20 30  40]
        ]
        :ipo [[0 6] [1 6] [2 6] [3 6] [4 6] [5 6]]
        :yellow [
           [0 0]
           [1 0] [1 1]
           [2 0] [2 1] [2 2]
                 [3 1] [3 2] [3 3]
                       [4 2] [4 3]
                             [5 3] [5 4]
                                   [6 4]
                                   [7 4] [7 5]
                                         [8 5] [8 6]
                                               [9 6]
        ]
        :brown[
           [3 0]
           [4 0] [4 1]
                 [5 1] [5 2]
                       [6 2] [6 3]
                             [7 3]
                                   [8 4]
                                         [9 5]
                                               [10 6]
        ]
        :red[
           [5 0]
           [6 0] [6 1]
           [7 0] [7 1] [7 2]
                 [8 1] [8 2] [8  3]
                       [9 2] [9  3] [9 4]
                             [10 3] [10 4] [10 5]
        ]
    }
}
  )
