(ns mon18.core
    (:require
      [reagent.core :as r]
      [reagent.dom :as rdom]
      [clojure.string :as s]
      [mon18.gc1830 :as g]
      ))

(def nbsp "\u00a0")
;; -------------------------
;; Global state

(defonce gamestate (r/atom {:started false
                       ; :players {}
                        :players
 {"Player0"
  {:id "Player0",
   :name "Player0",
   :cash 0,
   :holdings {}},
  "Player1"
  {:id "Player1",
   :name "Player1",
   :cash 0,
   :holdings {}},
  "Player2"
  {:id "Player2",
   :name "Player2",
   :cash 0,
   :holdings {}}}
                        }))
(defonce history (r/atom (list {:action {:action :init} :gamestate @gamestate})))
(defonce redo (r/atom '()))

(defonce uistate (r/atom {:active [:players "Player0"] :action nil :entity nil}))

(declare sr-handle-keycode)
(declare or-handle-keycode)

(def gamecfg g/gamecfg-init)

(defn js-value [from] (-> js/document (.getElementById (str from)) .-value))
; Stolen from SO
(defn indices-of [f coll]
  (keep-indexed #(if (f %2) %1 nil) coll))

(defn first-index-of [f coll]
  (first (indices-of f coll)))

(defn find-index [value coll]
  (first-index-of #(= % value) coll))

(defn co-id-of [gamestate co-name]
  (first (first (filter #(= (:name (second %)) (name co-name)) (:share-cos gamestate)))))

(defn share-size [share] (get {"p" 2 "s" 1} share))
(def id-type {"P" :players "C" :share-cos "B" :bank})
(defn path-to [id]
  (if (seqable? id)
    (let [type-of (get id-type (first id))]
      (if (nil? type-of)
        [id]
        [type-of id]))
    [id]))

(defn shares-of [gamestate entity of]
  (let [share-path (if (= (last entity) :ipo)
                    (concat entity [of])
                    (concat entity [:holdings of]))]
    (reduce (fn [sum share]
           (+ sum (share-size share))) 0 (get-in gamestate share-path))))

(defn percent-of [gamestate entity of]
  (let [co (get-in gamestate [:share-cos of])
        size (:size co)
        share-percent (/ 100 size)
        ]
    (* (shares-of gamestate entity of) share-percent)))


(defn cmv-compare [[a-price [a-row a-col]] [b-price [b-row b-col]] a-order b-order]
  "Compare two 'cmv' values to determine which order they are in.
  a 'cmv' value is [price [r c]] where r is increasing down and c is increasing right
  a 'higher' value is higher price, or more right or more up, then order of arival.
  Sort order here seems reversed, but it is an high-to-low sorted."
  (first (filter
           (partial not= 0)
           (list (compare b-price a-price)
                 (compare b-col a-col)
                 ; lower is better
                 (compare a-row b-row)
                 ; order in stack
                 (compare a-order b-order)
                 )))

  )
(defn co-run-compare [gamestate a b]
  (let [
        a-cmv (get-in gamestate [:share-cos a :cmv])
        b-cmv (get-in gamestate [:share-cos b :cmv])
        a-loc (flatten [:market :grid (second a-cmv) :markers])
        a-stack (get-in gamestate a-loc)
        a-order (find-index a a-stack)
        b-order (find-index b a-stack)
        ]
   (cmv-compare a-cmv b-cmv a-order b-order))
  )

(defn co-run-order [gamestate]
  (sort (partial co-run-compare gamestate)
        (get-in gamestate [:operation :share-cos-order])))

(defn move-marker [gamestate co-path to-path]
  (let [co (get-in gamestate co-path)
        ]
   (-> gamestate
    (update-in (flatten [:market :grid to-path :markers]) concat [(:id co)])
    (cond->
      (:cmv co) (update-in (flatten [:market :grid (second (:cmv co)) :markers])
                           (fn [co-list] (remove #(= (:id co) %) co-list))))
    (assoc-in [:share-cos (:id co) :cmv]
              [(get-in gamestate (flatten [:market :grid to-path :cmv])) to-path])
    (#(assoc-in % [:operation :share-cos-order] (co-run-order %)))
    )))

; These refer to each other, so need to pre-declare them.
(declare market-up market-down market-left market-right)

(defn market-up [gamestate from-loc]
      (let [loc (update-in from-loc [0] dec)]
        (if (get-in gamestate (concat [:market :grid] loc))
          loc
          from-loc)))
          ;(market-right gamestate from-loc))))  ; Slide right off top

(defn market-down [gamestate from-loc]
      (let [
            loc (update-in from-loc [0] inc)]
        (if (get-in gamestate (concat [:market :grid] loc))
          loc
          from-loc)))   ; floor sticky

(defn market-left [gamestate from-loc]
      (let [loc (update-in from-loc [1] dec)]
        (if (get-in gamestate (concat [:market :grid] loc))
          loc
          (market-down gamestate from-loc))))

(defn market-right [gamestate from-loc]
      (let [loc (update-in from-loc [1] inc)]
        (if (get-in gamestate (concat [:market :grid] loc))
          loc
          (market-up gamestate from-loc))))


(defn move-cmv [gamestate of-path motions]
  (reduce
      (fn [loc motion]
          (case motion
            (:up)    (market-up    gamestate loc)
            (:down)  (market-down  gamestate loc)
            (:left)  (market-left  gamestate loc)
            (:right) (market-right gamestate loc)))
        (get-in gamestate (concat of-path [ :cmv 1]))
        motions))


(defn sr-end-motion [gamestate co-id co]
  (if (= 0 (+ (shares-of gamestate [:share-cos co-id :ipo] co-id)
              (shares-of gamestate [:bank "Bank" :holdings] co-id)))
     (move-marker gamestate [:share-cos co-id] (move-cmv gamestate [:share-cos co-id] [:up]))
    gamestate))

(defn float-company [g co-id]
  (-> g (assoc-in [:share-cos co-id :floated] true)))

(defn co-float? [g of]
          (let [co (get-in g [:share-cos of])]
            (if (and (not (:floated co))
                     (>= (- 100 (:cap-float gamecfg))
                         (percent-of g [:share-cos of :ipo] (:id co))))
              (float-company g (:id co))
              g)))

(defn dividend-motion [g of-path fraction payout]
 (let [motion (if (= 0 payout) [:left] (if (= 0.5 fraction) [] [:right]))
       loc (move-cmv g of-path motion)]
    (move-marker g of-path loc)))

(defn transfer-cash [gamestate from to amount debt?]
  (prn from to amount debt?)
  (if (= 0 amount)
    gamestate
    (if (>= (get-in gamestate (concat from [ :cash])) amount)
      (-> gamestate
            (update-in (concat from [ :cash]) #(- % amount))
            (update-in (concat to [ :cash]) #(+ % amount)))
      false)))

(defn pay-shares [gamestate of-id dividend]
  (let [of-path (path-to of-id)
        of (get-in gamestate of-path)
        payees (concat (map (fn [[player-id _]][:players player-id]) (:players gamestate))
                       (map (fn [[co-id _]][:share-cos co-id]) (:share-cos gamestate)))
        payouts (map (fn [payee] [payee (shares-of gamestate payee of-id)]) payees)
        ; pool pays where? snoonan 3/20
        pool-payout [of-path (shares-of gamestate [:bank "Bank"] of-id)]
        ; IPO pays where? snoonan 3/20
        ipo-payout [[:bank "Bank"] (shares-of gamestate (concat of-path [:ipo]) of-id)]
        all-payouts (conj payouts pool-payout ipo-payout)
        run-per-share (int (/ (+ dividend (dec (:size of))) (:size of)))
        ]
    (reduce (fn [res [ payee payout]]
              (transfer-cash res of-path payee
                 (* run-per-share payout) true))
            gamestate
            all-payouts)))

(defn transfer-asset [gamestate from to of inx]
  (let [
        from-list (get-in gamestate (conj from of))
        asset (nth from-list inx)
        ]
    (-> gamestate
            (update-in (conj to of) conj asset )
            (assoc-in  (conj from of) (concat (take inx from-list)(drop (inc inx) from-list))))))

(defn transfer-cert [gamestate from to of inx price-per-share payee]
  (let [from-list (get-in gamestate (conj from of))
        asset (nth from-list inx)
        total-price (* price-per-share (share-size asset)) ]
  (-> gamestate
      (transfer-asset from to of inx)
      (transfer-cash (take 2 to) payee total-price false)
      (co-float? of))))

(defn par-company [gamestate co-path par-path]
  (-> gamestate
         (assoc-in (conj co-path :par) (get-in gamestate (flatten [:market :grid par-path :cmv])))))

;; -------------------------
;; HTML helpers
(defn safe-atoi [string]
  (let [i (js/parseInt string)
        failed (js/isNaN i)]
    (if failed 0 i)))

(defn stop-event [e]
  (let [c (.-charCode e)]
    (if (and (>= c 48) (<= c 57))  ; \0 <= c <= \9
      (.stopPropagation e)
      (.focus (.getElementById js/document "frame")))))

;; -------------------------
;; Views

(defn networth [player]
  ; Make this sum share value as well
  (reduce (fn [sum [co-id shares]]
            (+ sum (reduce (fn [sum share]
                             (+ sum (* (get-in @gamestate [:share-cos co-id :cmv 0])
                                    (share-size share))))
                   0 shares)))
          (:cash player) (:holdings player)))

(defn mark-market-with-tag
  [gamestate tag]
  (reduce (fn [result coord]
            (update-in result (flatten [:market :grid coord :type]) conj (name tag)))
          gamestate (get-in gamecfg [:market tag])))

(declare sr-start or-start)

(defn or-end [gamestate]
  (let [g  (update-in gamestate [:share-cos] #(reduce-kv (fn [r i p] (assoc r i (dissoc p :operated))) {} % ))]
  (if (= (get-in g [:operation :or])
         (get-in g [:operation :or-limit]))
    (sr-start g)
    (or-start g))))

(defn or-next-actor [gamestate]
  (let [
        actor-list (get-in gamestate [:operation :share-cos-order])
        active (get-in gamestate [:operation :active])
        next-active (first
                      (filter #(and
                                 (get-in gamestate [:share-cos % :floated])
                                 (not (get-in gamestate [:share-cos % :operated])))
                              actor-list))
        is-last (nil? next-active)
        _ (prn actor-list active next-active is-last )
        ]

      (if is-last
         (or-end gamestate)
         (-> gamestate
          (assoc-in [:operation :active] next-active)
          (assoc-in [:operation :phase] :lay-track)
          (update-in [:share-cos next-active]  dissoc :operated)
          (update-in [:share-cos next-active]  dissoc :track-cost)
          (update-in [:share-cos next-active]  dissoc :station-cost)))))

(defn map-privates [gamestate result f]
  (reduce (fn [res [actor-type [id actor]]]
            (reduce (fn [r private]
                      (f r actor-type id actor private))
                    res (:privates actor)))
          result (partition 2 (concat (interleave (repeat :players) (:players gamestate))
                                         (interleave (repeat :share-cos) (:share-cos gamestate))))))

(defn pay-privates [gamestate] 
  (map-privates gamestate gamestate (fn [g actor-type id actor private] (transfer-cash g [:bank "Bank"] [actor-type id] (:income private) true))))

(defn or-start [gamestate]
  (let [_ (prn gamestate)
        ]
    (-> gamestate
        (update-in [:operation :or] inc)
        (update-in [:operation] dissoc :active)
        (update-in [:share-cos] #(reduce-kv (fn [r i p] (assoc r i (dissoc p :operated))) {} % ))
        (pay-privates)
        (or-next-actor)
        ))
  )

(defn sr-end [gamestate]
  (let [
        player-list (get-in gamestate [:operation :player-order])
        active (get-in gamestate [:operation :active])
        inx (count (take-while (partial not= active) player-list))
        or-in-set 2
        ]
    (-> gamestate
      (assoc-in [:operation :deal] inx)
      (assoc-in [:operation :or] 0)
      (assoc-in [:operation :or-limit] or-in-set)
      (update-in [:players] #(reduce-kv (fn [r i p] (assoc r i (dissoc p :sold-co))) {} % ))
      ; (game variant) move down for shares in pool
      ; Move up sold out

      (#(reduce-kv (fn [g co-id co] (sr-end-motion g co-id co) ) % (:share-cos gamestate)))
      (or-start)
      ))
  )
(defn sr-next-actor [gamestate]
  (let [
        player-list (get-in gamestate [:operation :player-order])
        active (get-in gamestate [:operation :active])
        inx (count (take-while (partial not= active) player-list))
        next-inx (if active
                   (if (= (inc inx) (count player-list))
                    0
                    (inc inx))
                   (get-in gamestate [:operation :deal]))
        next-active (nth player-list next-inx)
        is-active (or (get-in gamestate [:players active :bought])
                      (get-in gamestate [:players active :sold]))
        passed (count (filter (fn [[i p]] (:passed p)) (:players gamestate)))
        passed-next (if (not is-active) (inc passed) passed)
        is-last (= (count player-list) passed-next)
        _ (prn player-list active inx next-active passed passed-next is-active is-last )
        ]

    (-> gamestate
      (assoc-in [:players (or active next-active) :passed] (not is-active))
      (assoc-in [:operation :active] next-active)
      (assoc-in [:operation :phase] :sell-buy)
      (update-in [:operation] dissoc :sellat)
      (update-in [:players next-active]  dissoc :bought)
      (update-in [:players next-active]  dissoc :sold)
      (update-in [:players next-active]  dissoc :passed)
      ((fn [g] (if is-last (sr-end g) g)))
      )))

(defn sr-start [gamestate]
     (-> gamestate
        (update-in [:operation :sr] inc)
        (update-in [:operation] dissoc :active)
        (update-in [:players] #(reduce-kv (fn [r i p] (assoc r i (dissoc p :sold-co))) {} % ))
        (update-in [:players] #(reduce-kv (fn [r i p] (assoc r i (dissoc p :passed))) {} % ))
        (sr-next-actor)))

(defn start-game [gamestate]
  (let [players (:players gamestate)
        n-players (count players)
        player-cash (get-in gamecfg [:player-start-cash n-players])
        bank-cash (get-in gamecfg [:bank n-players]) ]
      (-> gamestate
                       (assoc :started true)
                       (assoc :operation {:sr 0 :phase :auction})
                       (assoc-in [:operation :player-order] (keys (:players gamestate)))
                       (assoc-in [:operation :deal] 0)
                       (assoc-in  [:market :grid] (mapv (fn [r] (mapv (fn [c] {:cmv c} ) r)) (get-in gamecfg [:market :grid])))
                       ((fn [g]
                          (reduce
                            (fn [result tag] (mark-market-with-tag result tag))
                            g
                            (filter #(not (= % :grid)) (keys (get-in gamecfg [:market]))))))
                       (assoc :pars (mapv #(hash-map :cmv  (get-in gamecfg (concat [:market :grid] %)) :loc %) (get-in gamecfg [:market :ipo])))
                       (assoc :bank {"Bank" {:cash bank-cash :broken false :holdings {}}})
                       (assoc :share-co-available (:share-cos gamecfg))
                       (assoc :paper-limit (get-in gamecfg [:paper-limit n-players]))
                       (assoc :privates (:privates gamecfg))
                       (assoc-in [:trains :stock] (get-in gamecfg [:trains :stock]))
                       (assoc-in [:trains :trains] (apply concat (map-indexed #(repeat (:count %2) {:rank %1 :name (first (:train %2))}) (get-in gamecfg [:trains :stock]))))
                       ((fn [g] (reduce (fn [gamestate [id player]]
                                   (transfer-cash gamestate
                                                  [:bank "Bank"] [:players id]
                                                  player-cash false))
                                     g players)))
                       )))

(defn create-company [gamestate new-id]
  (let [co-id (str "Co" (count (:share-cos gamestate)))
        cos (:share-cos gamestate)
        id (co-id-of gamestate new-id)
        ]
  (if (nil? id)
    (assoc-in gamestate [:share-cos co-id]
              {:id co-id
               :name (get-in gamestate [:share-co-available new-id :name])
               :cash 0 :holdings {}
               :size 10
               :stations (get-in gamecfg [:share-cos new-id :stations ])
               :ipo {co-id (seq (:shares gamecfg))}}
              )
    gamestate
    ))
  )

(defn start-company [gamestate new-id par-path]
  (let [market (get-in gamestate (flatten [:market :grid par-path]))
        cmv (:cmv market) ]
        (as-> gamestate $
          (create-company $ new-id)
          (assoc-in $ [:share-cos (co-id-of $ new-id) :par] cmv)
          (update-in $ [:share-co-available] dissoc new-id )
          (update-in $ [:operation :share-cos-order] concat [(co-id-of $ new-id)])
          (move-marker $ [:share-cos (co-id-of $ new-id)] par-path)
          (transfer-cash $ [:bank "Bank"] [:share-cos (co-id-of $ new-id)] (* cmv (nth (:share-count gamecfg) 0)) true)
          )))

(defn add-player [gamestate] (-> gamestate
                                     (assoc-in [:players (str "Player" (count (:players gamestate)))]
                                        {:id (str "Player" (count (:players gamestate)))
                                         :name (str "Player" (count (:players gamestate)))
                                         :cash 0 :holdings {}})))

(defn update-player-name [gamestate id new-name]
  (-> gamestate
                       (assoc-in [:players id :name] new-name)))

(defn update-active-player [gamestate id]
  (-> gamestate
                       (assoc :entity id)
                       (assoc :focus nil)
                       (assoc :action nil)))

(defn showtrainstock [train]
  [:tr
   [:td (str (:train train))]
   [:td (str (:price train))]
   [:td (:count train)]])

(defn showtrains [trains]
  (comment [:table
   [:thead
    [:tr
      [:th "Train"]
      [:th "$"]
      [:th "Count"]
      [:th "Rusts"]
      [:th "Phase"]
      ]]
   [:tbody
    (doall (for [train trains]
      ^{:key (:train train)} [showtrainstock train]))]])
  [:div  "Trains:"
   (doall (for [train trains] [:span (str (:name train))]))]
  )

(defn showgrid [grid]
  [:table
   [:tbody
    (doall (for [[row-id row] (map-indexed vector grid)]
      ^{:key (str "grid" row-id)} [:tr
      (doall (for [[col-id col] (map-indexed vector row)]
        (if (< 0 (:cmv col))
            [:td {
                  :key (str "grid" row-id "_" col-id)
                  :class (s/join " " (flatten [(:type col) "grid"]))}
            (:cmv col)
              (doall (map (fn [mark] [:span {:key (str "mark_" mark)
                                      :class (str (get-in @gamestate [:share-cos mark :name]) " marker")
                                      } (get-in @gamestate [:share-cos mark :name])])
                   (:markers col)))]
        ^{:key (str "grid" row-id "_" col-id)} [:td])))]))]])

(defn showholding [entity location co-id shares]
  (let []
     [:span {:key (str location co-id) }
            (reduce
              (fn [[shares special] share]
                (list
                  (+ shares (share-size share))
                  (if (not= share "s")
                    (conj special share)
                    special)))
              (list 0 (list))
              shares)]))

(defn showplayer [player started active share-cos operation]
(if (nil? player)
  [:thead
      [:tr
       [:th {:row-span 2} "Name"]
       [:th {:row-span 2} "$"]
       [:th {:col-span ( inc (count share-cos ))} "Holdings"]
       [:th {:row-span 2} "Privates"]
       [:th {:row-span 2} "Net worth"]]
      [:tr  (do  (for [[id co] share-cos] ^{:key (str "p-owned" id)} [:th {:class (:name co)} (:name co)])) [:th "Limits"] ]]
  [:tr
    (if started
      [:td
       (if (= active (:id player))
          {:class "active"}
          {:on-click #(update-active-player @gamestate (:id player))})
       (when (= (:id player) (nth (:player-order operation) (:deal operation))) "*")
       (:name player)
      ]
      [:td [:input.text {:value (:name player) :on-change #(update-player-name @gamestate (:id player) (-> % .-target .-value)) }]])
    [:td (:cash player) ]
    (doall
      (for [[id _] share-cos]
        ^{:key (str (:name player) id "holding")}
        [:td (if (some #(= id (get-in % [1])) (:sold-co player)) {:class "sold"} {})
            (when (= active (:id player))
                [:input
                  {:tabIndex 0
                   :autoFocus (= id (first (keys share-cos)))
                   :style {:width "0em"}
                   :value id :onKeyPress sr-handle-keycode}]
                )
              (showholding player :holdings id (get-in player [:holdings id] ))]))
    [:td
      (s/join "+"
              [ (reduce
                (fn [r paper] (+ r paper)) 0
                (map #(count (second %)) (:holdings player)))])
      "/"
      (:paper-limit @gamestate)
     ]
    [:td (doall (for [private (:privates player)] ^{:key (str (:name player) (:id private))} [:span (:id private) nbsp] ))]
    [:td (networth player)]]))

(defn data-entry [value active step]
  (if active
           [:input
              {:tabIndex 0
               :autoFocus true
               :type "number"
               :step step
               :style {:width "5em"}
               :default-value (js/parseInt value)
               :onKeyPress or-handle-keycode
               }]
            value)
  )
(defn showco [co operation share-cos]
 (let [active (:active operation)
       is-active (= active (:id co))
       cls (when is-active "active")
       is-track (= :lay-track (:phase operation))
       is-station (= :lay-station (:phase operation))
       is-run (= :run (:phase operation))
       is-train (= :buy-train (:phase operation))
       ]
  (if (nil? co)
   [:thead
      [:tr
       [:th {:row-span 2} "Name"]
       [:th {:row-span 2} "$"]
       (when (:company-hold-shares gamecfg)
         [:th {:col-span ( inc (count share-cos ))} "Holdings"])
       [:th {:row-span 2} "Privates"]
       [:th {:row-span 2} "CMV"]
       [:th {:row-span 2} "Track"]
       [:th {:row-span 2} "Station"]
       [:th {:row-span 2} "Run"]
       [:th {:row-span 2} "Trains"]
       [:th {:row-span 2} "IPO"]
       [:th {:row-span 2} "Par"]
       ]
       (when (:company-hold-shares gamecfg)
         [:tr (do (for [[id co] share-cos] ^{:key (str "p-owned" id)} [:th {:class (:name co)} (:name co)])) ] )
      ]
  [:tr (when (:operated co) {:class "operated" })
    [:td {:class cls} [:span {:style {:color "transparent"} :class (str (:name co))} "----"]
         (:name co)]
    [:td (:cash co) ]
    (when (:company-hold-shares gamecfg)
      (doall
        (for [[id _] (get-in @gamestate [:share-cos])]
          ^{:key (str (:name co) id "holding")}
          [:td [:span (if is-active
                        {:tabIndex 0
                         :autoFocus false
                         :value id :onKeyPress or-handle-keycode}
                        {})
                (showholding co :holdings id (get-in co [:holdings id] ))]]))
           )
    [:td (doall (for [private (:privates co)] ^{:key (str (:name co) (:id private))} [:span (:id private) nbsp] ))]
    [:td (first (:cmv co)) " @" (second (second (:cmv co))) ","
                               (first (second (:cmv co)))]
    [:td [data-entry (:track-cost co 0) (and is-active is-track) 20] ]
    [:td [data-entry (:station-cost co 0) (and is-active is-station) 20] ]
    [:td [data-entry (:run co 0) (and is-active is-run) 10] ]
    [:td (when (and is-active is-train)
                [:input
                  {:tabIndex 0
                   :autoFocus true
                   :style {:width "0em"}
                   :onKeyPress or-handle-keycode
                   }]
                )
            (doall (for [train (:trains co)] [:span (str (:name train))])) ]
    [:td {:class (if (:floated co)
                     "floated"
                     "not-floated")
          :key (str co "ipo") }
         (doall (map (fn [[co-id shares]] (showholding co :ipo co-id shares)) (:ipo co)))]
    [:td (:par co)]
    ])))

(defn showbank [bank share-cos]
       ^{:key "bank"} [:tr
        [:td "Bank" [:br] (if (:broken bank) "Broken" "Operating")]
        [:td (:cash bank) ]
    (doall
      (for [[id _] share-cos]
        ^{:key (str "bank" id "holding")}
        [:td (showholding bank :holdings id (get-in bank [:holdings id] ))]))
    ])

(defn showlog [history redo]
  [:details {:open true :tabIndex -1}
    [:summary  "Game log" ]
        (for [[id log] (map-indexed (fn [i log] [i (:action log)]) (reverse history) )]
            ^{:key (str "h-" id)} [:details {:tabIndex -1} [:summary (str (:action log) )] (str log)])
        (for [[id log] (map-indexed (fn [i log] [i (:action log)]) redo )]
            ^{:key (str "r-" id)} [:details {:tabIndex -1 :class "future"} [:summary (str (:action log) )] (str log)])] )

(defn update-gamestate [g action]
     (case (:action action)
       (:start-game) (start-game g)
       (:private-auction)  (as-> g $
                             (reduce (fn [g {:keys [:id :winner :bid]}]
                               (-> g
                                   (transfer-cash
                                             [:players winner]
                                             [:bank "Bank"]
                                             bid false)
                                   (transfer-asset []
                                             [:players winner]
                                             :privates
                                             0  ; Pull out from :privates in the same order, so always index 0
                                             )))
                             g (:results action))
                             (sr-start $))
        (:par) (-> g (start-company (:active action) (:focus action)))
        (:buy-share) (-> g
                         ; TODO snoonan 3/20 
                         ; use focus to set inx and append :holdings or 0 and :ipo
                        (transfer-cert (conj (:entity action) (:focus action))
                                        (conj (:active action) :holdings)
                                        (second (:object action)) 0
                                      (if (= (:focus action) :ipo)
                                        (get-in g (conj (:object action) :par))
                                        (get-in g (conj (:object action) :cmv 0)))
                                      (if (= (:focus action) :ipo)   ; if inc-cap, pay to :object
                                        [:bank "Bank"]
                                        (:entity action)))
                        ;(assoc-in [:operation :phase] :sell)
                        (assoc-in (concat (:active action) [:bought]) true))
        (:sell-share) ((fn [g]
                        (let [
                              of (get-in g (:object action))
                              sellats (get-in g [:operation :sellat])
                              cmv (first (:cmv of))
                              ; new value first so existing sellat can overwrite
                              ; we are providing a default, but if the value already exists, use that one
                              new-sellats (merge {(:id of) cmv} sellats)
                              sell-cmv (get new-sellats (:id of))
                              ]
                        (-> g
                            (assoc-in [:operation :sellat] new-sellats)
                            (transfer-cert (conj (:active action) (keyword (:focus action)))
                                            (conj (:entity action) :holdings)
                                            (:id of) 0
                                            sell-cmv
                                            (:active action))
                            (update-in (concat (:active action) [:sold-co]) #(conj % (:object action)))
                            (assoc-in (concat (:active action) [:sold]) true)
                            ; todo: down on sale.  down if (= cmv sell-cmv)
                            (move-marker (:object action) (move-cmv g (:object action) [:down]))
                            ))) g)

        (:sr-pass) (sr-next-actor g)

        (:lay-track) (-> g
                        (assoc-in (concat ( :active action) [:track-cost]) (:focus action))
                        (assoc-in [:operation :phase] :lay-station)
                        (transfer-cash (:active action) [:bank "Bank"] (:focus action) false))
        (:lay-station) (-> g
                        (assoc-in (concat ( :active action) [:station-cost]) (:focus action))
                        (update-in (concat ( :active action) [:stations]) dec)
                        (assoc-in [:operation :phase] :run)
                        (transfer-cash (:active action) [:bank "Bank"] (:focus action) false))

        (:place-tile) (transfer-cash g (:active action) [:bank "Bank"] (:focus action) false)
        (:place-station) (-> g
                             (transfer-cash (:active action) [:bank "Bank"] (:focus action) false)
                             (update-in (conj (:active action) :stations) dec)
                             )
        (:run) (assoc-in g (conj (:active action) :run) (:focus action))
        (:dividend) (-> g
                        ; Pay company, then pay out dividend from that.
                        (transfer-cash [:bank "Bank"] (:active action) (get-in g (concat (:active action) [:run]) ) true) 
                        (pay-shares (second (:active action))
                                    (* (:focus action) (get-in g (conj (:active action) :run))))
                        (dividend-motion (:active action) (:focus action) (* (:focus action) (get-in g (conj (:active action) :run))))
                        (assoc-in [:operation :phase] :buy-train)
                        )
        (:buy-train) (as-> g $
                        (transfer-asset $ (:entity action) (:active action)
                                         :trains (:object action))
                        (apply transfer-cash  $
                                       (if (= [:trains] (:entity action))
                                         (let [train (nth (get-in $ [:trains :trains]) (:object action))
                                               rank (:rank train)
                                               type-record (get-in @gamestate [:trains :stock rank])
                                               type-index (find-index (:name train) (:train type-record))
                                               price (nth ( :price type-record) type-index)
                                               ]
                                       (list (:active action) (:entity action) price false))
                                       (list (:active action) (:entity action) (:focus action) false))
                                       ))

        (:or-pass)  (-> g
                        (assoc-in (conj (:active action) :operated) true)
                        (or-next-actor))

        (:buy-private) (-> g
                        (transfer-asset (:entity action) (:active action)
                                        :privates (:object action))
                        (transfer-cash (:active action) (:entity action) (:focus action) false))

        (:move-cmv) (move-marker g (:active action) (move-cmv g (:active action) (:focus action)))

        (:move-cash) (transfer-cash
                      g
                      (:active action) (:entity action)
                      (:focus action) false)

        (:move-asset) (transfer-asset g (:active action) (:entity action)
                                        (:object action) (:focus action))

        (:move-share) (transfer-cert g (:active action ) (:entity action)
                                      (:object action) 0
                                      0 [:bank "Bank"])))

(defn do-action [action]
  (prn action)
  (let [old-gamestate @gamestate
        new-gamestate (update-gamestate old-gamestate action) ]
   (if new-gamestate
     (do
         (compare-and-set! gamestate old-gamestate new-gamestate)
         (swap! history conj {:action action :gamestate new-gamestate})
         )
     false)))

(defn apply-undo []
  (when-not (= 1 (count @history))
    (swap! redo conj (first @history))
    (swap! history rest)
    (reset! gamestate (:gamestate (first @history)))))

(defn apply-redo []
  (do
    ;; Updates @gamestate and makes new @history
    (do-action (:action (first @redo)))
    (swap! redo rest)))

(defn skip-redo []
  (do (swap! redo rest)))

(defn drop-redo []
  (do (reset! redo '())))

( defn or-handle-keycode [e]
   (let [c (char (.-charCode e))
         active [:share-cos (get-in @gamestate [:operation :active])]
         phase (get-in @gamestate [:operation :phase])
         is-run (= phase :run)
         is-train (= phase :buy-train)
         is-last (= phase :buy-train)
         value (.. e -currentTarget -value)
         integer (if (empty? value) 0 (js/parseInt value))
        _ (prn c e integer)]
     (case c
         ("\r") (cond
                  is-last (do-action {:action :or-pass :active active})
                  is-run (do
                   (do-action {:action :run :active active :focus integer })
                   (do-action {:action :dividend :active active :focus 1.0}))
                 :else (do-action {:action (get-in @gamestate [ :operation :phase])
                             :active active
                             :focus integer}))
         ("-") (when is-run
                 (do
                   (do-action {:action :run :active active :focus integer })
                   (do-action {:action :dividend :active active :focus 0})))
         ("/") (when is-run
                 (do
                   (do-action {:action :run :active active :focus integer })
                   (do-action {:action :dividend :active active :focus 0.5})))
         ("+") (when is-run
                 (do
                   (do-action {:action :run :active active :focus integer })
                   (do-action {:action :dividend :active active :focus 1.0})))
         ("=") (when is-train
                  (do-action {:action :buy-train :active active :entity [:trains] :object 0 :focus 0}))
         false)))

( defn sr-handle-keycode [e]
   (let [c (char (.-charCode e))
         active [:players (get-in @gamestate [:operation :active])]
         phase (get-in @gamestate [:operation :phase])
         can-buy (or (= phase :sell-buy) (= phase :buy))
         can-sell (or (= phase :sell-buy) (= phase :sell))
         holding-entity [:share-cos (.. e -currentTarget (getAttribute "value")) ]
        _ (prn c e (. (. e -currentTarget) getAttribute "value"))]
     (case c
         ("+")  (if can-buy (do-action {:action :buy-share :active active
                            :entity [:bank "Bank"]
                            :object holding-entity
                            :focus :holdings}))
         ("=")  (if can-buy (do-action {:action :buy-share :active active
                            :entity holding-entity
                            :object holding-entity
                            :focus :ipo}))
         ("-")  (if can-sell (do-action {:action :sell-share :active active
                            :entity [:bank "Bank"]
                            :object holding-entity
                            :focus :holdings}))
         ("\r") (do-action {:action :sr-pass :active active })
         false)))

(defn maybe-ipo-path [value] (let [parts (s/split value "-")]
                               (if (= 2 (count parts))
                                 (conj (path-to (nth parts 1)) :ipo)
                                 (conj (path-to value) :holdings))))

(defn show-fixup-dialog [g]
  (let [players (:players g)
        companies (:share-cos g)
        trains (:trains (:trains g))
        ]
    [:div
    [:fieldset
     [:legend "Move "[:u "s"]"hares"]
     "Move share of "
     [:select {:id "fu-share-object" :accessKey "s"}
     (concat
       (doall (for [[id co] companies] [:option {:key id :value id} (:name co)]))
       )]
     "from "
     [:select {:id "fu-share-active"}
     (concat
       (doall (for [[id player] players] [:option {:key id :value id} (:name player)]))
       (when (:company-hold-shares gamecfg)
        (doall (for [[id co] companies] [:option {:key id :value id} (:name co)])))
       (doall (for [[id co] companies] [:option {:key id :value (str "ipo-" id)} (str "ipo " (:name co))]))
       )]
     "to "
     [:select {:id "fu-share-entity"}
     (concat
       (doall (for [[id player] players] [:option {:key id :value id} (:name player)]))
       (when (:company-hold-shares gamecfg)
        (doall (for [[id co] companies] [:option {:key id :value id} (:name co)])))
       (doall (for [[id co] companies] [:option {:key (str "ipo-" id) :value (str "ipo-" id)} (str "ipo " (:name co))]))
       )]
     [:span {:style {:float "right"}} [:button {
                                       :on-click #(do-action {:action :move-share
                                                              :active (maybe-ipo-path (js-value "fu-share-active"))
                                                              :entity (maybe-ipo-path (js-value "fu-share-entity"))
                                                              :object (js-value "fu-share-object")
                                                              })}
                                       "Apply" ]]]
    [:fieldset
     [:legend "Move " [:u "m"]"oney"]
     "Move $"
     [:input {:id "fu-money-focus" :accessKey "m"}]
     "from "
     [:select {:id "fu-money-active"}
     (concat
       (doall (for [[id player] players] [:option {:key id :value id} (:name player)]))
       (doall (for [[id co] companies] [:option {:key id :value id} (:name co)]))
       )]
     "to "
     [:select {:id "fu-money-entity"}
     (concat
       (doall (for [[id player] players] [:option {:key id :value id} (:name player)]))
       (doall (for [[id co] companies] [:option {:key id :value id} (:name co)]))
       )]
     [:span {:style {:float "right"}} [:button {
                                       :on-click #(do-action {:action :move-cash
                                                              :active (path-to (js-value "fu-money-active"))
                                                              :entity (path-to (js-value "fu-money-entity"))
                                                              :focus (js/parseInt ( js-value "fu-money-focus"))
                                                              })}
                                       "Apply" ]]]
    [:fieldset
     [:legend "Move " [:u "p"]"rivate"]
     "Move private of "
     [:select {:id "fu-private-focus" :accessKey "p"}
        (map-privates g '() (fn [result actor-type id actor private] (conj result [:option {:key (:id private) :value (:id private)} (:name private)]))) ]
     "from "
     [:select {:id "fu-private-active"}
     (concat
       (doall (for [[id player] players] [:option {:key id :value id} (:name player)]))
       (doall (for [[id co] companies] [:option {:key id :value id} (:name co)]))
       )]
     "to "
     [:select {:id "fu-private-entity"}
     (concat
       (doall (for [[id player] players] [:option {:key id :value id} (:name player)]))
       (doall (for [[id co] companies] [:option {:key id :value id} (:name co)]))
       )]
     [:span {:style {:float "right"}} [:button {
                                       :on-click #(do-action {:action :move-asset
                                                              :active (path-to (js-value "fu-private-active"))
                                                              :entity (path-to (js-value "fu-private-entity"))
                                                              :focus (first-index-of (fn [private] (= (:id private) (keyword (js-value "fu-private-focus"))))
                                                                                 (get-in g (conj (path-to (js-value "fu-private-active")) :privates)))
                                                              :object :privates
                                                              :value 0
                                                              })}
                                       "Apply"
                                       ]] 
     ]
    [:fieldset
     [:legend "Move " [:u "t"]"rain"]
     "Move "
     [:select {:id "fu-train-focus" :accessKey "t"}
      (concat
       ; Use this when we can remove the from line and use this to select train AND source
       ;(doall (for [[id co] companies] (for [train (distinct (:trains co))] [:option {:key (str id"-" (:rank train)) :value {:name train}} (:name co)"-" (:name train)])))
       (doall (for [train (distinct trains)] [:option {:key (:rank train) :value (:name train)} (:name train)]))
       )
      ]
     "train from "
     [:select {:id "fu-train-active"}
      [:option {:value :trains} "Supply"]
      [:option {:value "Bank"} "Pool"]
     (concat
       (doall (for [[id co] companies] [:option {:key id :value id} (:name co)]))
       )]
     "to "
     [:select {:id "fu-train-entity"}
      [:option {:value "Bank"} "Pool"]
     (concat
       (doall (for [[id co] companies] [:option {:key id :value id} (:name co)]))
       )]
     [:span {:style {:float "right"}} [:button {
                                       :on-click #(do-action {:action :move-asset
                                                              :active (path-to (js-value "fu-train-active"))
                                                              :entity (path-to (js-value "fu-train-entity"))
                                                              :focus (first-index-of (fn [v] (= (js-value "fu-train-focus") (:name v)))
                                                                                 (get-in g (conj (path-to (js-value "fu-train-active")) :trains)))
                                                              :object :trains
                                                              })}
                                      "Apply" ]]]
    [:fieldset
     [:legend "Move " [:u "C"]"MV marker"]
     "Move marker of "
     [:select {:id "fu-cmv-active" :accessKey "c"}
     (concat
       (doall (for [[id co] companies] [:option {:key id :value id} (:name co)]))
       )]
     [:span {:style {:float "right"}} [:button {
                                       :on-click #(do-action {:action :move-cmv
                                                              :active (path-to (js-value "fu-cmv-active"))
                                                              })}
                                       "Apply"]] ]
    [:button {:onClick #(swap! gamestate dissoc :dialog)} "Done"]

   ]))

(defn fixup-dialog []
  (swap! gamestate assoc :dialog show-fixup-dialog))

(defn handle-keycode [e]
  (let [c (char (.-charCode e))
        _ (prn c)]
    (case c
      ("u") (apply-undo)
      ("r") (apply-redo)
      ("s") (skip-redo)
      ("d") (drop-redo)
      ("*") (fixup-dialog)
         false)))

( defn auction-finish [e]
  (do-action {:action :private-auction
              :results
              (reduce (fn [r private]
                         (conj  r
                               {:id (:id private)
                                :winner (-> js/document
                                             (.getElementById (str "winner-" (:id private)))
                                             .-selectedOptions
                                             (aget 0)
                                             .-value)
                                :bid (js/parseInt (-> js/document
                                               (.getElementById (str "bid-" (:id private)))
                                               .-value)) }))
                        [] (:privates @gamestate))  }))

(defn show-auction-private [private players]
  (if (nil? private)
    [:tr 
      [:th "Private"]
      [:th "price"]
      [:th "income"]
      [:th "winner"]
      [:th "bid"]
      ]
    [:tr 
      [:td (:name private)]
      [:td (:price private)]
      [:td (:income private)]
      [:td [:select {:id (str "winner-" (:id private))}
            [:option "-- Select bidder --"]
            (for [[id player] players]
              [:option {:value id :key id} (:name player)]) ]]
      [:td [:input {:type "number" :id (str "bid-" (:id private)) :step 5 :default-value (:price private)}]]
      ]
    )
  )

(defn show-auction [privates players]
  [:div
  [:table
   [:thead
    (show-auction-private nil nil)]
   [:tbody
    (doall (for [private privates]
          ^{:key (:id private)} [show-auction-private private players]))]
   ]
  [:button {:onClick auction-finish} "Accept bids"]]
  )

(defn company-start-select []
  (let [
        co-name (keyword (-> js/document
                             (.getElementById "co-select")
                             .-selectedOptions
                             (aget 0)
                             .-value))
        par     (get-in @gamestate
                        [:pars  (js/parseInt (-> js/document
                                                (.getElementById "co-par")
                                                .-selectedOptions
                                                (aget 0)
                                                .-value)) :loc])
        ]
      (do-action {:action :par :active co-name :focus par})
      ))

(defn overview []
  (let [started (:started @gamestate)
        phase (get-in @gamestate [:operation :phase])
        active-player (get-in @gamestate [:operation :active])
        bank (get-in @gamestate [:bank "Bank"])
        share-cos (get-in @gamestate [:share-cos])
        ]
    [:div {:id "frame"
           :tabIndex -1
           }
     (if (contains? @gamestate :dialog)
      ((:dialog @gamestate) @gamestate) ; Just show the dialog, else show everything else
      [:div
       [:div (str (:operation @gamestate))]
        [:table {:id "players"}
         [showplayer nil nil nil share-cos nil]
         [:tbody
          (doall (for [[id player] (:players @gamestate)]
            ^{:key (str id)} [showplayer player started active-player share-cos (:operation @gamestate)]))
          [showbank (get-in @gamestate [:bank "Bank"]) share-cos]]]
      (if started
        (if (= phase :auction)
          [show-auction (:privates @gamestate) (:players @gamestate)]
        [:div
          [:div
             [:span [:select {:tabIndex -1
                              :id "co-select" }
                     [:option "-- select company --"]
                     (map (fn [[k v]] [:option {:value k :key k} (:name v)]) (:share-co-available @gamestate))]]
             [:span [:select {:tabIndex -1
                              :id "co-par" :on-change company-start-select } [:option "-- select par --"] (map-indexed (fn [i v] [:option {:value i :key (:cmv v)} (:cmv v)]) (:pars @gamestate))]]
             ]
          [:table {:id "share-cos"}
           [showco nil nil share-cos]
           [:tbody
            (doall
            (for [id (get-in @gamestate [:operation :share-cos-order])]
              ^{:key id} [showco (get share-cos id) (:operation @gamestate) share-cos]))]]
          [showtrains (get-in @gamestate [:trains :trains])]
          [showgrid (:grid (:market @gamestate))]
          ])
        [:div
          [:input {:type "button" :value "Add Player"
              :on-click #(swap! gamestate add-player)}]
          [:input {:type "button" :value "Start"
            :on-click #(do-action {:action :start-game})}]])
        [:div {:id "log"}
         [showlog @history @redo]
         ]])
        ]))


;; -------------------------
;; Initialize app

(def layout
  [:div
   [:player [:name :cash :holdings :networth]]
   [:share-cos [:name :tile :station :run :trains :cash :holdings :cmv :ipo :par]]
   [:market [:grid]]
   [:trains [:trains-render]]
   [:log [:log-render]]
   ]
  )

(defn mount-root []
  (rdom/render [overview] (.getElementById js/document "app")))

(defn init! []
  (mount-root)
  (-> js/document
      (.addEventListener "keypress" handle-keycode)
      )
  )
